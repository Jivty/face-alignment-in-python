from face_alignment import FaceAlignment, Settings

MANUAL_MODE = False  # Input index for test (with Terminal)
SAVE_MODE = True  # Auto-save results
SHOW_MODE = False  # Auto-show results


if __name__ == "__main__":
    settings = Settings()
    model = FaceAlignment(name="test_model", settings=settings)
    model.train()
    model.save()
    model.load()
    model.test(
        manual=MANUAL_MODE,
        saving_results=SAVE_MODE,
        showing_results=SHOW_MODE,
    )
