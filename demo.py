from face_alignment import FaceAlignment, Settings

MANUAL_MODE = True  # Input index for test (with Terminal)
SAVE_MODE = False  # Auto-save results
SHOW_MODE = True  # Auto-show results


if __name__ == "__main__":
    settings = Settings()
    model = FaceAlignment(name="model", settings=settings)
    model.load()
    model.test(
        manual=MANUAL_MODE,
        saving_results=SAVE_MODE,
        showing_results=SHOW_MODE,
    )
