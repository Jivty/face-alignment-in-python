from typing import List, Tuple

import numpy as np
import numpy.typing as npt
from cv2 import COVAR_COLS, calcCovarMatrix

from face_alignment.bounding_box import BoundingBox


def get_mean_shape(
    shapes: List[npt.NDArray[np.float64]],
    bounding_box: List[BoundingBox],
) -> npt.NDArray[np.float64]:
    result = np.zeros((shapes[0].shape[0], 2))
    for shape, b_box in zip(shapes, bounding_box):
        result += project_shape(shape, b_box)
    return result / len(shapes)


def project_shape(
    shape: npt.NDArray[np.float64],
    bounding_box: BoundingBox,
) -> npt.NDArray[np.float64]:
    temp = np.zeros((shape.shape[0], 2))
    temp[:, 0] = 2 * (shape[:, 0] - bounding_box.centroid_x) / bounding_box.width
    temp[:, 1] = 2 * (shape[:, 1] - bounding_box.centroid_y) / bounding_box.height
    return temp


def re_project_shape(
    shape: npt.NDArray[np.float64],
    bounding_box: BoundingBox,
) -> npt.NDArray[np.float64]:
    temp = np.zeros((shape.shape[0], 2))
    temp[:, 0] = shape[:, 0] * bounding_box.width / 2 + bounding_box.centroid_x
    temp[:, 1] = shape[:, 1] * bounding_box.height / 2 + bounding_box.centroid_y
    return temp


def similarity_transform(
    shape1: npt.NDArray[np.float64],
    shape2: npt.NDArray[np.float64],
) -> Tuple[npt.NDArray[np.float64], float]:
    rotation = np.zeros((2, 2))

    temp1 = shape1.copy()
    temp2 = shape2.copy()

    temp1[:, 0] -= np.mean(shape1[:, 0])
    temp1[:, 1] -= np.mean(shape1[:, 1])
    temp2[:, 0] -= np.mean(shape2[:, 0])
    temp2[:, 1] -= np.mean(shape2[:, 1])

    covariance1, _ = calcCovarMatrix(temp1, mean=None, flags=COVAR_COLS)
    covariance2, _ = calcCovarMatrix(temp2, mean=None, flags=COVAR_COLS)

    s1 = np.sqrt(np.linalg.norm(covariance1))
    s2 = np.sqrt(np.linalg.norm(covariance2))
    scale = s1 / s2
    temp1 /= s1
    temp2 /= s2

    num = np.sum(temp1[:, 1] * temp2[:, 0] - temp1[:, 0] * temp2[:, 1])
    den = np.sum(temp1[:, 0] * temp2[:, 0] + temp1[:, 1] * temp2[:, 1])

    norm = np.sqrt(num**2 + den**2)
    sin_theta = num / norm
    cos_theta = den / norm
    rotation[0, 0] = cos_theta
    rotation[0, 1] = -sin_theta
    rotation[1, 0] = sin_theta
    rotation[1, 1] = cos_theta

    return rotation, scale


def calculate_covariance(v1: List[float], v2: List[float]) -> float:
    return np.mean((v1 - np.mean(v1)) * (v2 - np.mean(v2)))
