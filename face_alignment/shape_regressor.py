from typing import IO, List

import numpy as np
import numpy.typing as npt

from face_alignment.bounding_box import BoundingBox
from face_alignment.fern_cascade import FernCascade
from face_alignment.utils import get_mean_shape, project_shape, re_project_shape


class ShapeRegressor:
    def __init__(self) -> None:
        # for convenience
        __first_level_num: int
        __landmark_num: int
        __fern_cascades: List[FernCascade]
        __training_shapes: List[npt.NDArray[np.float64]]
        __bounding_box: List[BoundingBox]

    def train(
        self,
        images: List[npt.NDArray[np.int_]],
        ground_truth_shapes: List[npt.NDArray[np.float64]],
        bounding_box: List[BoundingBox],
        first_level_num: int,
        second_level_num: int,
        candidate_pixel_num: int,
        fern_pixel_num: int,
        initial_num: int,
    ) -> None:
        print("Start training")
        self.__bounding_box = bounding_box
        self.__training_shapes = ground_truth_shapes
        self.__first_level_num = first_level_num
        self.__landmark_num = ground_truth_shapes[0].shape[0]

        augmented_images = []
        augmented_bounding_box = []
        augmented_ground_truth_shapes = []
        current_shapes = []

        for i, image in enumerate(images):
            for j in range(initial_num):
                index = i
                while index == i:
                    index = np.random.randint(low=0, high=len(images))
                augmented_images.append(image)
                augmented_ground_truth_shapes.append(ground_truth_shapes[i])
                augmented_bounding_box.append(bounding_box[i])
                current_shapes.append(
                    re_project_shape(
                        project_shape(ground_truth_shapes[index], bounding_box[index]),
                        bounding_box[i],
                    )
                )

        self.__mean_shape = get_mean_shape(ground_truth_shapes, bounding_box)

        self.__fern_cascades = [FernCascade() for _ in range(first_level_num)]

        for i in range(first_level_num):
            print(f"Training fern cascades: {i + 1} out of {first_level_num}")
            prediction = self.__fern_cascades[i].train(
                augmented_images,
                current_shapes,
                augmented_ground_truth_shapes,
                augmented_bounding_box,
                self.__mean_shape,
                second_level_num,
                candidate_pixel_num,
                fern_pixel_num,
                i + 1,
                first_level_num,
            )
            for j, predict in enumerate(prediction):
                current_shapes[j] = re_project_shape(
                    predict
                    + project_shape(current_shapes[j], augmented_bounding_box[j]),
                    augmented_bounding_box[j],
                )

    def write(self, fout: IO) -> None:
        fout.write(f"{self.__first_level_num}\n")
        fout.write(f"{self.__mean_shape.shape[0]}\n")
        for i in range(self.__landmark_num):
            fout.write(f"{self.__mean_shape[i, 0]} {self.__mean_shape[i, 1]} ")
        fout.write("\n")
        fout.write(f"{len(self.__training_shapes)}\n")
        for i in range(len(self.__training_shapes)):
            fout.write(
                " ".join(
                    list(
                        map(
                            str,
                            (
                                int(self.__bounding_box[i].start_x),
                                int(self.__bounding_box[i].start_y),
                                int(self.__bounding_box[i].width),
                                int(self.__bounding_box[i].height),
                                self.__bounding_box[i].centroid_x,
                                self.__bounding_box[i].centroid_y,
                            ),
                        )
                    )
                )
            )
            fout.write("\n")
            for j in range(self.__landmark_num):
                fout.write(
                    f"{self.__training_shapes[i][j, 0]} {self.__training_shapes[i][j, 1]} "
                )
            fout.write("\n")
        for fern_cascade in self.__fern_cascades:
            fern_cascade.write(fout)

    def read(self, fin: IO) -> None:
        self.__first_level_num = int(fin.readline().strip())
        self.__landmark_num = int(fin.readline().strip())
        self.__mean_shape = np.zeros((self.__landmark_num, 2))
        shapes = list(map(float, fin.readline().strip().split()))
        self.__mean_shape[:, 0] = shapes[::2]
        self.__mean_shape[:, 1] = shapes[1::2]

        training_num = int(fin.readline().strip())
        self.__training_shapes = []
        self.__bounding_box = []

        for i in range(training_num):
            box_args = list(map(float, fin.readline().strip().split()))
            self.__bounding_box.append(
                BoundingBox(
                    start_x=box_args[0],
                    start_y=box_args[1],
                    width=box_args[2],
                    height=box_args[3],
                )
            )
            self.__bounding_box[-1].centroid_x = box_args[4]
            self.__bounding_box[-1].centroid_y = box_args[5]

            temp1 = np.zeros((self.__landmark_num, 2))

            shapes = list(map(float, fin.readline().strip().split()))
            temp1[:, 0] = shapes[::2]
            temp1[:, 1] = shapes[1::2]
            self.__training_shapes.append(temp1)

        self.__fern_cascades = [FernCascade() for _ in range(self.__first_level_num)]
        for fern_cascade in self.__fern_cascades:
            fern_cascade.read(fin)

    def predict(
        self,
        image: npt.NDArray[np.int_],
        bounding_box: BoundingBox,
        initial_num: int,
    ) -> npt.NDArray[np.float64]:
        result = np.zeros((self.__landmark_num, 2))
        for _ in range(initial_num):
            index = np.random.randint(low=0, high=len(self.__training_shapes))
            current_bounding_box = self.__bounding_box[index]
            current_shape = re_project_shape(
                project_shape(self.__training_shapes[index], current_bounding_box),
                bounding_box,
            )
            for j in range(self.__first_level_num):
                prediction = self.__fern_cascades[j].predict(
                    image, bounding_box, self.__mean_shape, current_shape
                )
                current_shape = re_project_shape(
                    prediction + project_shape(current_shape, bounding_box),
                    bounding_box,
                )
            result += current_shape

        return 1 / initial_num * result
