from typing import List

import cv2
import numpy as np
import numpy.typing as npt

from face_alignment.bounding_box import BoundingBox
from face_alignment.settings import Settings
from face_alignment.shape_regressor import ShapeRegressor


class FaceAlignment:
    def __init__(self, name: str, settings: Settings) -> None:
        self.name = name
        self.settings = settings
        print("Initializing model with settings:")
        print(settings.json(indent=4))
        self.model = ShapeRegressor()

    def load_images(self, data_path: str, img_num: int) -> List[npt.NDArray[np.int_]]:
        images = []
        for i in range(img_num):
            images.append(cv2.imread(f"{data_path}{str(i+1)}.jpg", 0))
        return images

    def load_bounding_box(self, box_path: str) -> List[BoundingBox]:
        box_lines = []
        bounding_box = []
        with open(box_path) as f:
            box_lines = f.readlines()
        for line in box_lines:
            box_args = list(map(int, line.split()))
            bounding_box.append(
                BoundingBox(
                    start_x=box_args[0],
                    start_y=box_args[1],
                    width=box_args[2],
                    height=box_args[3],
                )
            )
        return bounding_box

    def load_shapes(self, shape_path: str) -> List[npt.NDArray[np.float64]]:
        shapes_lines = []
        ground_truth_shapes = []
        with open(shape_path) as f:
            shapes_lines = f.readlines()
        for i in range(self.settings.TRAIN_IMG_NUM):
            shape_args = list(map(float, shapes_lines[i].split()))
            temp = np.zeros((self.settings.LANDMARK_NUM, 2))
            for j in range(self.settings.LANDMARK_NUM):
                temp[j, 0] = shape_args[j]
                temp[j, 1] = shape_args[self.settings.LANDMARK_NUM + j]
            ground_truth_shapes.append(temp)
        return ground_truth_shapes

    def train(self) -> None:
        print("\n---   ---\n")
        print("LOADING DATA...")
        print("\n---\n")
        print("Loading train images...")
        images = self.load_images(
            self.settings.TRAIN_DATA_PATH, self.settings.TRAIN_IMG_NUM
        )
        print("Done: train images loaded")

        print("\n---\n")
        print("Loading bounding boxes...")
        bounding_box = self.load_bounding_box(self.settings.TRAIN_BOX_FILENAME)
        print("Done: bounding boxes loaded")

        print("\n---\n")
        print("Loading ground truth shapes...")
        ground_truth_shapes = self.load_shapes(self.settings.TRAIN_SHAPE_FILENAME)
        print("Done: ground truth shapes loaded")

        print("\n---\n")
        print("DATA LOADED\nTRAINING MODEL...")
        print("\n---\n")
        self.model.train(
            [np.asarray(image[:, :]) for image in images],
            ground_truth_shapes,
            bounding_box,
            self.settings.TRAIN_FIRST_LEVEL_NUM,
            self.settings.TRAIN_SECOND_LEVEL_NUM,
            self.settings.TRAIN_CANDIDATE_PIXEL_NUM,
            self.settings.TRAIN_FERN_PIXEL_NUM,
            self.settings.INITIAL_NUMBER,
        )
        print("\n---\n")
        print("MODEL TRAINED")
        print("\n---   ---\n")

    def test(
        self,
        manual: bool = False,
        saving_results: bool = False,
        showing_results: bool = False,
    ) -> List[npt.NDArray[np.float64]]:
        print("\n---   ---\n")
        print("LOADING DATA...")
        print("\n---\n")
        print("Loading test images...")
        images = self.load_images(
            self.settings.TEST_DATA_PATH, self.settings.TEST_IMG_NUM
        )
        print("Done: test images loaded")

        print("\n---\n")
        print("Loading bounding boxes...")
        bounding_box = self.load_bounding_box(self.settings.TEST_BOX_FILENAME)
        print("Done: bounding boxes loaded")

        print("\n---\n")
        print("DATA LOADED\nTESTING MODEL...")
        print("\n---\n")

        index = -1
        results = []
        if manual:
            print(
                f"Indexes can be from 1 to {self.settings.TEST_IMG_NUM}. Input any other for exit."
            )
        while index < self.settings.TEST_IMG_NUM:
            if manual:
                print("Input index:")
                index = int(input())
                index -= 1
            else:
                index += 1
            if index < 0 or index >= self.settings.TEST_IMG_NUM:
                break

            current_shape = self.model.predict(
                np.asarray(images[index][:, :]),
                bounding_box[index],
                self.settings.INITIAL_NUMBER,
            )

            test_image_1 = cv2.cvtColor(images[index].copy(), cv2.COLOR_GRAY2RGB)
            for i in range(self.settings.LANDMARK_NUM):
                test_image_1 = cv2.circle(
                    img=test_image_1,
                    center=(int(current_shape[i, 0]), int(current_shape[i, 1])),
                    radius=3,
                    color=(0, 255, 0),
                    thickness=-1,
                    lineType=8,
                    shift=0,
                )

            if saving_results:
                cv2.imwrite(
                    f"{self.settings.RESULTS_PATH}{self.name}_{str(index + 1)}.jpg",
                    test_image_1,
                )
            if showing_results:
                cv2.imshow(f"Result for {str(index + 1)}.jpg", test_image_1)
                cv2.waitKey(0)
            results.append(current_shape)

        print("\n---\n")
        print("END OF TESTING MODEL")
        print("\n---   ---\n")
        return results

    def load(self) -> None:
        print("\n---   ---\n")
        print("Loading model...")
        with open(f"{self.settings.MODEL_PATH}{self.name}.txt") as fin:
            self.model.read(fin)
        print("Done: model loaded successfully")
        print("\n---   ---\n")

    def save(self) -> None:
        print("\n---   ---\n")
        print("Saving model...")
        with open(f"{self.settings.MODEL_PATH}{self.name}.txt", "w") as fout:
            self.model.write(fout)
        print("Done: model saved successfully")
        print("\n---   ---\n")
