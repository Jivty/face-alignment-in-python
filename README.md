# FaceAlignmentInPython

## Description

This program is a Python reimplementation of algorithms in paper "Face Alignment by Explicit Regression" by Cao et al. This program can be used to train models for detecting facial keypoints, and it is extremely fast both in training and testing.

The code from the [GitHub repo of sai-bi](https://github.com/sai-bi/FaceAlignment) was taken as a basis.

Examples of results are in the folder "results".

## Dataset

The COFW dataset can be obtained [here](https://drive.google.com/drive/folders/1EZcdz3dCT359IFhj8iWd4miG2EnvAYD0?usp=sharing). It contains 1345 training images, 507 testing images, and each image has 29 landmarks.

## Details

#### Environment variables
* `TRAIN_DATA_PATH`: default: `"data/trainingImages/"`
* `TRAIN_BOX_FILENAME`: default: `"data/boundingbox.txt"`
* `TRAIN_SHAPE_FILENAME`: default: `"data/keypoints.txt"`
* `TEST_DATA_PATH`: default: `"data/testImages/"`
* `TEST_BOX_FILENAME`: default: `"data/boundingbox_test.txt"`
* `MODEL_PATH`: default: `"model/"`
* `RESULTS_PATH`: default: `"results/"`
* `TRAIN_IMG_NUM`: default: `1345`
* `TEST_IMG_NUM`: default: `507`
* `TRAIN_CANDIDATE_PIXEL_NUM`: default: `400`
* `TRAIN_FERN_PIXEL_NUM`: default: `5`
* `TRAIN_FIRST_LEVEL_NUM`: default: `10`
* `TRAIN_SECOND_LEVEL_NUM`: default: `500`
* `LANDMARK_NUM`: default: `29`
* `INITIAL_NUMBER`: default: `20`

#### CMD

`$ python main.py`

`$ python demo.py`

#### Development

##### Install dependencies:
```bash
pip install -r requirements_dev.txt
```

##### Pre-commit hooks:
```bash
pre-commit install
pre-commit autoupdate
pre-commit install-hooks
```

Test pre-commit hooks:

```bash
pre-commit run --all-files -v
```

##### Running locally
Recommended way to run locally for ad-hoc development tests:

```bash
source .env
python main.py
```
*Note:* You might want to run demo.py for interactive testing.

## Reference papers

[Face Alignment by Explicit Shape Regression](http://research.microsoft.com/pubs/192097/cvpr12_facealignment.pdf)
